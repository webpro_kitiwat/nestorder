import { IsPositive, MinLength } from 'class-validator';

export class CreateCustomerDto {
  @MinLength(8)
  name: string;

  @IsPositive()
  age: number;

  @MinLength(10)
  tel: string;

  @MinLength(1)
  gender: string;
}
